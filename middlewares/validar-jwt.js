const {response, request} = require('express')
const jwt = require('jsonwebtoken');
const usuario = require('../models/usuario');
const Usuario = require('../models/usuario');

const validarJWT = async(req = request, res = response, next)=>{
    const token = req.header('x-token');
    if (!token) {
        return res.status(401).json({
            msg:'No hay token en la peticion'
        })
    } 
    try {

       const {uid}=  jwt.verify(token, process.env.secretOrPrivateKey);
    //leer el usuario que corresponde al uid {}extraer solo ese dato en especifico!
       const usuario= await Usuario.findById(uid);

       //Validacion si el usuari no existe
       if (!usuario) {
        return res.status(401).json({
            msg:'Token no valido - usuario no existe dB'
        })
       }


       //Validacion para verificar si el uid esta activo
       if (!usuario.estado) {
        return res.status(401).json({
            msg: 'Token no valido - usuario con estado: false'
        })
        
       } 
       
       req.usuario=usuario;

        next();
        
    } catch (error) {
        console.log(error);
        res.status(401).json({
            msg: 'Token invalido'
        })
    }

    console.log(token);
}

module.exports = {validarJWT}